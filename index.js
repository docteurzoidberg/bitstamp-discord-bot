var Discord = require("discord.js");
var Bitstamp = require("bitstamp");

var config = require('./config.js');

var bitstampclient = new Bitstamp();
var discordwebhookclient = new Discord.WebhookClient(config.id, config.token);

var lastAnnouncementTicker = 0;
var trending = 0;

function checkTicker() {

    bitstampclient.ticker('btceur', function(err, ticker) {
        
        if(err) {
            console.log("Error: " + err.message + ", skipping");
            return;
        }

        var diffPrice = Math.abs(ticker.last - lastAnnouncementTicker);

        if(diffPrice < 100) {
            //console.log("Diff only " + diffPrice + ", skipping");
            return;
        }
            
        if(lastAnnouncementTicker>0) {
            trending = ticker.last < lastAnnouncementTicker ? -1 : 1;                 
        } else {
            lastAnnouncementTicker = ticker.last;
            return;
        }
        
        var delta = trending == 0 ? '' : (trending > 0 ?  '+' : '-') + Math.round(diffPrice);
        var date = new Date();
        var hour = date.getHours();
        var min  = date.getMinutes();
        var sec  = date.getSeconds();
        var time = ((hour < 10 ? "0" : "") + hour) + ':' + ((min < 10 ? "0" : "") + min) + ':' + (sec < 10 ? "0" : "") + sec;
        var trendingEmoji = (trending > 0? ":chart_with_upwards_trend:" : trending < 0 ? ":chart_with_downwards_trend:" : "");
        var message = `${trendingEmoji} ${ticker.last} EUR (${delta}) at ${time}`
        
        console.log(message);
        
        discordwebhookclient
            .sendMessage(message)
            .then(function() {
                lastAnnouncementTicker = ticker.last;
            })
            .catch(function(err) {
                console.error(err);
            });
    })
}

checkTicker();
setInterval(checkTicker, 60 * 1000);
 
discordwebhookclient
    .sendMessage("Bot just started")
    .then(function() {})
    .catch(function(err) {
        console.error(err);
    });